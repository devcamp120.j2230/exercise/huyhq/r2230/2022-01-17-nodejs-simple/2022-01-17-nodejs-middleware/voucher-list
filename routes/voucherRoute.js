const express = require("express");
const {
    getAllVoucher,
    getAVoucher,
    postAVoucher,
    putAVoucher,
    deleteAVoucher
} = require("../middlewares/voucherMiddleware");

const voucherRoute = express.Router();


voucherRoute.get("/",getAllVoucher,(req,res)=>{
    res.status(200).json({
        message: "Get All Vouchers!",
    })
})

voucherRoute.get("/:id",getAVoucher,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Get Voucher id = "+id,
    })
})

voucherRoute.post("/:id",postAVoucher,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Create Voucher id = "+id,
    })
})

voucherRoute.put("/:id",putAVoucher,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Update Voucher id = "+id,
    })
})

voucherRoute.delete("/:id",deleteAVoucher,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Delete Voucher id = "+id,
    })
})

module.exports = {voucherRoute};